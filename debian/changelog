phpunit-recursion-context (7.0.0-1) experimental; urgency=medium

  * Upload new major to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Sat, 08 Feb 2025 22:49:12 +0100

phpunit-recursion-context (6.0.2-3) unstable; urgency=medium

  * Source-only upload for testing migration

 -- David Prévot <taffit@debian.org>  Fri, 10 Jan 2025 12:06:31 +0100

phpunit-recursion-context (6.0.2-2) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 11

 -- David Prévot <taffit@debian.org>  Fri, 10 Jan 2025 09:32:40 +0100

phpunit-recursion-context (6.0.2-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Use PHPStan instead of Psalm
  * Prepare release

  [ Markus Staab ]
  * Use more efficient `spl_object_id()` over `spl_object_hash()`

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Thu, 18 Jul 2024 11:14:00 +0900

phpunit-recursion-context (6.0.0-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Start development of next major version
  * Remove dynamic property
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Sat, 03 Feb 2024 14:08:05 +0100

phpunit-recursion-context (5.0.0-1) experimental; urgency=medium

  * Upload new major version to experimental

  [ Sebastian Bergmann ]
  * Drop support for PHP 8.0
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Revert "Track version 4 for now (Bookworm?)"
  * Update copyright (years and license)
  * Ship upstream security notice
  * Use phpunit 10 for tests

 -- David Prévot <taffit@debian.org>  Sat, 04 Feb 2023 00:02:06 +0100

phpunit-recursion-context (4.0.5-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Prepare release

  [ David Prévot ]
  * Track version 4 for now (Bookworm?)
  * Update standards version to 4.6.2, no changes needed.
  * Update copyright (years)

 -- David Prévot <taffit@debian.org>  Fri, 03 Feb 2023 16:02:11 +0100

phpunit-recursion-context (4.0.4-2) unstable; urgency=medium

  * Simplify gbp import-orig (and check signature)
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Mark package as Multi-Arch: foreign
  * Update Standards-Version to 4.6.1
  * Install /u/s/pkg-php-tools/{autoloaders,overrides} files

 -- David Prévot <taffit@debian.org>  Wed, 06 Jul 2022 20:08:04 +0200

phpunit-recursion-context (4.0.4-1) unstable; urgency=medium

  * Upload to unstable in sync with PHPUnit 9

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Update watch file format version to 4.
  * Update Standards-Version to 4.5.1

 -- David Prévot <taffit@debian.org>  Sun, 20 Dec 2020 15:15:40 -0400

phpunit-recursion-context (4.0.3-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Rename main branch to debian/latest (DEP-14)
  * Set Rules-Requires-Root: no.

 -- David Prévot <taffit@debian.org>  Wed, 30 Sep 2020 10:03:59 -0400

phpunit-recursion-context (4.0.2-1) experimental; urgency=medium

  [ Sebastian Bergmann ]
  * Ignore tests etc. from archive exports
  * Support PHP 8 for https://github.com/sebastianbergmann/phpunit/issues/4325
  * Prepare release

  [ David Prévot ]
  * Document gbp import-ref usage
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test

 -- David Prévot <taffit@debian.org>  Sun, 28 Jun 2020 14:17:00 -1000

phpunit-recursion-context (4.0.0-1) experimental; urgency=medium

  * Upload version compatible with PHPUnit 9 to experimental

  [ Sebastian Bergmann ]
  * Bump copyright year
  * Drop support for PHP 7.0, PHP 7.1, and PHP 7.2
  * Prepare release

  [ David Prévot ]
  * debian/copyright: Update copyright (years)
  * debian/upstream/metadata:
    set fields Bug-Database, Bug-Submit, Repository, Repository-Browse
  * debian/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update standards version to 4.5.0

 -- David Prévot <taffit@debian.org>  Fri, 07 Feb 2020 19:28:11 -1000

phpunit-recursion-context (3.0.0-3) unstable; urgency=medium

  * Compatibility with recent PHPUnit (8)
  * Drop get-orig-source target
  * Use versioned copyright format URI.
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Contact, Name.
  * Move repository to salsa.d.o
  * Update Standards-Version to 4.4.0

 -- David Prévot <taffit@debian.org>  Fri, 23 Aug 2019 10:29:52 -1000

phpunit-recursion-context (3.0.0-2) unstable; urgency=medium

  * Upload to unstable, with the rest of the latest PHPUnit stack
  * Update Standards-Version to 4.1.1

 -- David Prévot <taffit@debian.org>  Sat, 25 Nov 2017 15:54:19 -1000

phpunit-recursion-context (3.0.0-1) experimental; urgency=medium

  * Upload version only compatible with latest PHPUnit stack to experimental

  [ Sebastian Bergmann ]
  * Prepare release

  [ David Prévot ]
  * Update copyright (years)
  * Update Standards-Version to 4.1.0
  * Don’t use default upstream build system

 -- David Prévot <taffit@debian.org>  Tue, 22 Aug 2017 15:27:41 -1000

phpunit-recursion-context (1.0.2-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.7
  * Rebuild with latest pkg-php-tools for the PHP 7.0 transition

 -- David Prévot <taffit@debian.org>  Sat, 05 Mar 2016 10:41:47 -0400

phpunit-recursion-context (1.0.2-1) unstable; urgency=medium

  [ Bob Weinand ]
  * Remove unnecessary ext/hash dependency

 -- David Prévot <taffit@debian.org>  Tue, 08 Dec 2015 20:13:09 -0400

phpunit-recursion-context (1.0.1-1) unstable; urgency=medium

  [ Sebastian Bergmann ]
  * Fix CS/WS issues

  [ David Prévot ]
  * Simplify test calls

 -- David Prévot <taffit@debian.org>  Sun, 25 Oct 2015 13:01:44 -0400

phpunit-recursion-context (1.0.0-1) unstable; urgency=low

  * Initial release

 -- David Prévot <taffit@debian.org>  Sun, 08 Feb 2015 18:06:23 -0400
